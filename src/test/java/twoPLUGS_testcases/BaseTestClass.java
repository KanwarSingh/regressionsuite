package twoPLUGS_testcases;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;

public class BaseTestClass{

	WebDriver driver;
	String baseUrl;
	Actions action;
	
	//Method to declare wait using Thread.sleep
	public void staticWait(Integer secs)
	{
		try {
			Thread.sleep(secs* 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	//Method to initialize driver i.e. chrome or firefox with parameter "browser" 
	public WebDriver InitializeDriver(String browser) throws IOException {
		if (browser.equalsIgnoreCase("chrome")) {
			System.setProperty("webdriver.chrome.driver", "./Drivers\\chromedriver.exe");
			driver = new ChromeDriver();
		} else {
			System.setProperty("webdriver.gecko.driver", "./Drivers\\geckodriver.exe");
			driver = new FirefoxDriver();
		}
		
		driver.manage().timeouts().implicitlyWait(8, TimeUnit.SECONDS);

		return driver;
	}
	
	
	//Method to Log in to website "qatest.twoplugs.com"
	public void logInTwoPlugs(WebDriver driver, String username, String password, String urlToLogin) {

		//driver goes to the URL of website to login: qatest.twoplugs.com
		driver.get(urlToLogin);
		
		//Click on LOG IN button
		driver.findElement(By.xpath("//span[@class=\"help\" and contains(text(),'LOG in')]")).click();
		
		//Enter username and password to login
		driver.findElement(By.xpath("//input[@id='signInEmail']")).sendKeys(username);
		driver.findElement(By.xpath("//input[@id='signInPassword']")).sendKeys(password);
		
		
		staticWait(1);
		
		//Click on "LOG In" button
		driver.findElement(By.xpath("//span[contains(text(),'LOG IN')]")).click();
		staticWait(3);
	}

	//Method to quit driver
	public void quitDriver(WebDriver driver) {
		driver.quit();
		driver=null;

	}

	//Method to logout from qatest.twoplugs.com by clicking on "Sign out" button from dropdown
	public void logOut(WebDriver driver, Actions action) {
		
		action = new Actions(driver);
		driver.manage().window().maximize();    //Maximizing window
		action.moveToElement(driver.findElement(By.xpath("//span[@class='caret']"))).build().perform();
		driver.findElement(By.xpath("//span[contains(text(),'Sign Out')]")).click();
		
	}
	
	//Method to logout by directly navigating to a URL(used when unable to logout by clickin on SignOut button)
	public void logOut(WebDriver driver) {
		driver.navigate().to("https://qatest.twoplugs.com/signout");
		}
	
}
