//TP_010 Testing whether he can bid the service by clicking on Let's negotiate button

package twoPLUGS_testcases;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;

import static org.testng.Assert.assertEquals;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.AfterTest;

public class TP_010 extends BaseTestClass {
	WebDriver driver;
	WebDriverWait wait;
	String baseURL;

	@Test
	@Parameters({ "browser", "searchName" })
	public void bidService(String browser, String searchName) {
		// click on search icon
		driver.findElement(By.xpath("//span[@class='w-icons-search']")).click();

		// Enter service you want to search for, click search
		WebElement searchInput = driver.findElement(By.xpath("//input[@id='searchInput']"));
		searchInput.sendKeys(searchName);
		driver.findElement(By.xpath("//span[contains(text(),'SEARCH')]")).click();

		// Click on the service you want to bid for from search results
		driver.findElement(By.xpath("//i[@class='tps tps-9']")).click();

		// click on let's negotiate
		wait.until(ExpectedConditions
				.visibilityOf(driver.findElement(By.xpath("//span[contains(text(),'Let`s negotiate')]"))));
		driver.findElement(By.xpath("//span[contains(text(),'Let`s negotiate')]")).click();

		// accept sale agreement and submit bid
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//div[@id='agreeterm-styler']"))));
		driver.findElement(By.xpath("//div[@id='agreeterm-styler']")).click();
		driver.findElement(By.xpath("//button[@id='contract_send']")).click();

		// Waiting for confirmation and storing its text in a string
		wait.until(
				ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//h3[contains(text(),'Bidding Sent')]"))));
		String result = driver.findElement(By.xpath("//h3[contains(text(),'Bidding Sent')]")).getText();

		// assertion to verify testcase
		Assert.assertEquals(result, "Bidding Sent");

		// Printing results
		if (result.contains("Bidding Sent")) {
			System.out.println("Testcase TP_010 is pass in " + browser);
		} else {
			System.out.println("Testcase TP_010 is fail in " + browser);
		}
	}

	@BeforeMethod
	public void verifyHomepage() {
		String expectedURL = "https://qatest.twoplugs.com/home";
		String actualURL = driver.getCurrentUrl();

		//Assertion to verify that we are on User's homepage after successfully logging in
		assertEquals(actualURL, expectedURL);
		System.out.println("You are on user's homepage");
	}

	@BeforeTest
	@Parameters({ "browser", "username", "password", "urlToLogin" })
	public void beforeTest(String browser, String username, String password, String urlToLogin) throws IOException {

		// Calling "InitializeDriver(browser)" method from BaseTest class
		driver = InitializeDriver(browser);

		// Initializing wait
		wait = new WebDriverWait(driver, 30);

		//Calling "logInTwoPlugs(driver, username, password, urlToLogin)" method from BaseTest Class to login
		logInTwoPlugs(driver, username, password, urlToLogin);

	}

	@AfterTest
	public void afterTest() {

		// Calling quitDriver(driver) method to quit the browser after executing the test
		quitDriver(driver);
	}

}
