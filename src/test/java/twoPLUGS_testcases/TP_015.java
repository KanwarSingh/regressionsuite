//TP_015 Testing whether he can  reset his password by clicking into forgot password

package twoPLUGS_testcases;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;

import static org.testng.Assert.assertEquals;

import java.io.IOException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.AfterTest;

public class TP_015 extends BaseTestClass {
	WebDriver driver;

	@Test
	@Parameters({"browser","resetEmail"})
	public void resetPassword(String browser, String resetEmail) {
		//Click on Log In button
		driver.findElement(By.xpath("//body[@class='main-template']/div[@class='wrapper']/header/div[@class='container']/ul[@class='control-bar']/li/a[@class='btn border']/span[1]")).click();
		
		//Click on "Forgot your Password?"
		driver.findElement(By.xpath("//a[contains(text(),'FORGOT YOUR PASSWORD?')]")).click();
		
		//Enter email and click "RESET"
		WebElement email = driver.findElement(By.xpath("//input[@placeholder='Enter your email address']"));
		email.sendKeys(resetEmail);
		staticWait(2);
		driver.findElement(By.xpath("//span[contains(text(),'reset')]")).click();
		staticWait(2);
		
		//Storing text of pop-up in String 		
		WebElement popUp = driver.findElement(By.xpath("//div[contains(text(),'Check your email for instructions on how to reset ')]"));
		String result = popUp.getText();
		
		//assertion to verify pop-up message
		Assert.assertEquals(result, "Check your email for instructions on how to reset your password" );
		
		//Printing results
		if (result.contains("reset")) {
			System.out.println("Testcase TP_015 is pass in "+browser);
		} else {
			System.out.println("Testcase TP_015 is fail in "+browser);
		}

	}

	@BeforeMethod
	public void verifyLandingPage() {
		String expected = "https://qatest.twoplugs.com/";
		String actual = driver.getCurrentUrl();
		System.out.println("You are on twoPLUGS landing page.");
		//Assertion to verify Landing page
		assertEquals(actual, expected);
	}

	@BeforeTest
	@Parameters({ "browser", "urlToLogin" })
	public void beforeTest(String browser, String urlToLogin) throws IOException {
		
		// Calling "InitializeDriver(browser)" method from BaseTest class
		driver = InitializeDriver(browser);
		
		//Navigating to twoPlugs landing page
		driver.get(urlToLogin);

	}

	@AfterTest
	public void afterTest() {
		
		// Calling quitDriver(driver) method to quit the browser after executing the test
		quitDriver(driver);
	}

}
