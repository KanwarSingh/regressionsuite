//TP_019 Testing whether a user can change the network

package twoPLUGS_testcases;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;

import static org.testng.Assert.assertEquals;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.AfterTest;

public class TP_019 extends BaseTestClass {
	WebDriver driver;
	WebDriverWait wait;

	@Test
	@Parameters({ "browser" })
	public void editProfile(String browser) {
		// Go to Profile using Actions class
		driver.manage().window().maximize();
		Actions goTo = new Actions(driver);
		goTo.moveToElement(driver.findElement(By.xpath("//span[@class='caret']"))).build().perform();

		driver.findElement(By.xpath("//span[contains(text(),'Profile')]")).click();

		// click on Edit profile
		driver.findElement(By.xpath("//a[contains(text(),'Edit Profile')]")).click();

		wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//label[contains(text(),'First Name')]")));

		// change State
		if (browser.equalsIgnoreCase("chrome")) {

			driver.findElement(By.xpath("//div[@id='stateDropdown-styler']//div[@class='jq-selectbox__trigger']"))
					.click();

			WebElement selectState = driver.findElement(By.xpath("//li[contains(text(),'Colorado')]"));
			wait.until(ExpectedConditions.visibilityOf(selectState));

			selectState.click();

		}

		else {
		
			WebElement province = driver.findElement(By.xpath("//div[@id='stateDropdown-styler']//div[@class='jq-selectbox__trigger-arrow']"));
			wait.until(ExpectedConditions.elementToBeClickable(province));
			province.click();
			WebElement provinceDropdown = driver
					.findElement(By.xpath("//div[@id='stateDropdown-styler']//div[@class='jq-selectbox__dropdown']"));
			wait.until(ExpectedConditions.visibilityOf(provinceDropdown));

			driver.findElement(By.xpath("//li[contains(text(),'Colorado')]")).click();
		}
		
		//Click on "Save changes" button
		WebDriverWait myWait = new WebDriverWait(driver, 25);
		
		myWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[contains(text(),'SAVE CHANGES')]")));

		driver.findElement(By.xpath("//span[contains(text(),'SAVE CHANGES')]")).click();

		//Waiting for changes to be saved
		myWait.until(ExpectedConditions.urlToBe("https://qatest.twoplugs.com/profile/joy"));

		//Assertion to verify that changes are reflected in profile
		String result = driver.findElement(By.xpath("//li[@class='wide']")).getText();
		
		System.out.println(result);
		
		Assert.assertEquals(true, result.contains("Colorado"));
		
		//Printing results
		if (result.contains("Colorado")) {
			System.out.println("Testcase TP_019 is pass in "+browser);
		} else {
			System.out.println("Testcase TP_019 is fail in "+browser);
		}
	}

	@BeforeMethod
	public void beforeMethod() {
		String expectedURL = "https://qatest.twoplugs.com/home";
		String actualURL = driver.getCurrentUrl();
		
		//Assertion to verify that we are on User's homepage after successfully logging in
		System.out.println("You are on Joy's homepage");
		assertEquals(actualURL, expectedURL);

	}

	@BeforeTest
	@Parameters({ "browser", "username", "password", "urlToLogin" })
	public void beforeTest(String browser, String username, String password, String urlToLogin) throws IOException {
		
		//Calling "InitializeDriver(browser)" method from BaseTest class
		driver = InitializeDriver(browser);
		
		//Calling "logInTwoPlugs(driver, username, password, urlToLogin)" method from BaseTest Class to login
		logInTwoPlugs(driver, username, password, urlToLogin);

	}

	@AfterTest
	public void afterTest() {
		
		//Calling quitDriver(driver) method to quit the browser after executing the test
		quitDriver(driver);
		}

}
