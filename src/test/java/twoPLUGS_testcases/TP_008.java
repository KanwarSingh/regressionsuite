//TP_008 Testing the functionality creating /updating/deleting a need

package twoPLUGS_testcases;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;

import static org.testng.Assert.assertEquals;

import java.io.IOException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.AfterTest;

public class TP_008 extends BaseTestClass {
	WebDriver driver;
	String baseURL;
	String goToURL = "https://qatest.twoplugs.com/home";
	WebDriverWait wait;
	static String profileLink;

	// Testing the functionality of Creating a need
	@Test(priority = 0)
	@Parameters({ "NeedName", "Description", "subCategory", "needPrice", "browser" })
	public void createNeed(String NeedName, String Description, String subCategory, String needPrice, String browser) {

		// Click on "Create" and then click on "Need"
		driver.findElement(By.xpath("//div[@class='create']")).click();
		driver.findElement(By.xpath("//a[contains(text(),'Need')]")).click();

		// Enter Need Title
		WebElement title = driver.findElement(By.xpath("//input[@id='name']"));
		title.sendKeys(NeedName);

		// Enter Need description
		WebElement description = driver.findElement(By.xpath("//textarea[@id='description']"));
		description.sendKeys(Description);

		// Select Need Category
		driver.findElement(By.xpath("//div[@id='category_id-styler']")).click();
		WebElement selectCategory = driver.findElement(By.xpath("//li[contains(text(),'Home & Child')]"));
		selectCategory.click();

		// Select Need Subcategory
		if (browser.equalsIgnoreCase("firefox")) {
			WebElement subCat = driver.findElement(By.xpath("//div[@id='subcategory_id-styler']"));
			wait.until(ExpectedConditions.elementToBeClickable(subCat));
			subCat.click();

			WebElement selectsubCategory = driver.findElement(By.xpath("//li[contains(text(),'" + subCategory + "')]"));
			System.out.println(selectsubCategory.getText());
			selectsubCategory.click();
		}

		// Enter need Price
		WebElement price = driver.findElement(By.xpath("//input[@id='price']"));
		price.sendKeys(needPrice);

		// Click on "Create"
		driver.findElement(By.xpath("//span[contains(text(),'create')]")).click();
		// driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);

		// Wait for Confirmation POp-up and store its text in a string "result"
		wait.until(ExpectedConditions
				.visibilityOf(driver.findElement(By.xpath("//div[contains(text(),'Need has been added')]"))));
		String result = driver.findElement(By.xpath("//div[contains(text(),'Need has been added')]")).getText();

		// Assertion to Verify Pop-up
		Assert.assertEquals(result, "Need has been added");

		// Printing Results
		if (result.contains("Need has been added")) {
			System.out.println("Create need successful in " + browser);
		} else {
			System.out.println("Create need failed in " + browser);
		}
	}

	// Testing the functionality of Updating a need
	@Test(priority = 1)
	@Parameters({ "updateNeedName", "browser" })
	public void updateNeed(String updateNeedName, String browser) {

		// Using Actions class to move to an element that displays a dropdown menu upon
		// moving to it
		Actions action = new Actions(driver);
		WebElement goTo = driver.findElement(By.xpath("//span[@class='caret']"));
		WebElement goToProfile = driver.findElement(By.xpath("//span[contains(text(),'Profile')]"));
		action.moveToElement(goTo).click().build().perform();
		goToProfile.click();

		// Storing URL of profile in a static string "profileLink"
		profileLink = driver.getCurrentUrl();

		// go to "washing clothes" need
		driver.findElement(By.xpath("//a[contains(text(),'washing clothes')]")).click();

		// Click on "Edit"
		WebElement edit = driver.findElement(By.xpath("//a[@class='pull-right edit-link']"));
		edit.click();

		// Update Need title
		WebElement updateTitle = driver.findElement(By.xpath("//input[@id='name']"));
		updateTitle.clear();
		updateTitle.sendKeys(updateNeedName);

		// Click on "Save"
		driver.findElement(
				By.xpath("//ul[@class='line-btn pull-right']//button[@class='btn btn-success w-btn-success']")).click();
		// driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);

		// Waiting for confirmation pop-up and storing text in a string "result"
		wait.until(ExpectedConditions
				.visibilityOf(driver.findElement(By.xpath("//div[contains(text(),'Need has been updated')]"))));
		String result = driver.findElement(By.xpath("//div[contains(text(),'Need has been updated')]")).getText();

		// Assertion to verify pop-up
		Assert.assertEquals(result, "Need has been updated");

		// Printing results
		if (result.contains("updated")) {
			System.out.println("Need updated in " + browser);
		} else {
			System.out.println("Need not updated in " + browser);
		}

		// Waiting for pop-up to be invisible
		wait.until(ExpectedConditions
				.invisibilityOfElementLocated(By.xpath("//div[contains(text(),'Need has been updated')]")));
	}

	// Testing the functionality of Deleting a need
	@Test(priority = 2)
	@Parameters({ "browser" })
	public void deleteNeed(String browser) {
		try {
			System.out.println("Profile link is " + profileLink);

			//// Navigating to profile by using static string "profileLink"
			driver.navigate().to(profileLink);

			// Calling staticWait(Integer secs) method from BaseTest class
			staticWait(4);

			// Click on Cross icon next to need name
			WebElement clickCross = driver.findElement(By.xpath("//*[@id=\"delete_service_link\"]/span"));
			wait.until(ExpectedConditions.visibilityOf(clickCross));
			clickCross.click();

			// Wait for pop-up to confirm delete and click on "I want to delete"
			wait.until(ExpectedConditions
					.visibilityOf(driver.findElement(By.xpath("//span[contains(text(),'I want to delete')]"))));
			driver.findElement(By.xpath("//span[contains(text(),'I want to delete')]")).click();
			// driver.manage().timeouts().implicitlyWait(6, TimeUnit.SECONDS);

			// Waiting for Pop-up to confirm delete and storing text in a string
			wait.until(ExpectedConditions
					.visibilityOf(driver.findElement(By.xpath("//div[contains(text(),'Need has been deleted')]"))));
			String result = driver.findElement(By.xpath("//div[contains(text(),'Need has been deleted')]")).getText();

			// Assertion to verify Pop-up
			Assert.assertEquals(result, "Need has been deleted");

			// Printing results
			if (result.contains("deleted")) {
				System.out.println("Need deleted in " + browser);
			} else {
				System.out.println("Delete need failed in " + browser);
			}
		} catch (Exception e) {
			Assert.assertEquals(true, false);
			System.out.println(e.getMessage());
			// TODO: handle exception
		}
	}

	@BeforeMethod
	public void beforeMethod() {
		String expectedURL = "https://qatest.twoplugs.com/home";
		String actualURL = driver.getCurrentUrl();

		// Assertion to verify that we are on User's homepage after successfully logging in
		assertEquals(actualURL, expectedURL);
		System.out.println("You are on user's homepage");
	}

	@AfterMethod
	public void afterMethod() {

		// Navigating to "https://qatest.twoplugs.com/home" after executing each test
		driver.get(goToURL);
	}

	@BeforeTest
	@Parameters({ "browser", "username", "password", "urlToLogin" })
	public void beforeTest(String browser, String username, String password, String urlToLogin) throws IOException {

		// Calling "InitializeDriver(browser)" method from BaseTest class
		driver = InitializeDriver(browser);

		// Initializing wait
		wait = new WebDriverWait(driver, 30);

		// Calling "logInTwoPlugs(driver, username, password, urlToLogin)" method from BaseTest Class to login
		logInTwoPlugs(driver, username, password, urlToLogin);

	}

	@AfterTest
	public void afterTest() {

		// Calling quitDriver(driver) method to quit the browser after executing the test
		quitDriver(driver);
	}

}
