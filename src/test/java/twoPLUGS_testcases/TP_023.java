//TP_023 Testing whether a user can change the PASSWORD through settings

package twoPLUGS_testcases;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;

import static org.testng.Assert.assertEquals;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.AfterTest;

public class TP_023 extends BaseTestClass {
	WebDriver driver;
	WebDriverWait wait;

	//Method to Change Password from "oldPassword" to "newPassword"
	public String password(String oldPassword, String newPassword) {
		
		//Enter Current Password
		WebElement currentPassword = driver.findElement(By.xpath("//input[@name=\"new_password_verify\"]"));
		currentPassword.sendKeys(oldPassword);

		//Enter new Password
		WebElement newPswd = driver.findElement(By.xpath("//input[@name=\"new_password\"]"));
		newPswd.sendKeys(newPassword);

		//Confirm new password
		WebElement confirmPassword = driver.findElement(By.xpath("//input[@name=\"confirm_password\"]"));
		confirmPassword.sendKeys(newPassword);

		//Click on "Save Changes"
		driver.findElement(By.xpath("//span[contains(text(),'SAVE CHANGES')]")).click();

		//Waiting for confirmation Message and storing its text in a String 
		WebElement displayMsg = driver.findElement(By.xpath("//div[contains(text(),'Your password has been updated')]"));
		
		//Assertion to verify Confirmation message
		Assert.assertEquals(true, displayMsg.isDisplayed());
		String result = displayMsg.getText();
		
		//Returning String in which text of confirmation message is stored
		return result;
	}

	@Test
	@Parameters({"browser"})
	public void changeEmail(String browser) {
		
		//Calling password(String oldPassword, String newPassword) method to change password
		String result=	password("test2plug", "test3plug");

		//Printing results
		if (result.contains("password has been updated")) {
			System.out.println("Testcase TP_023 is pass in "+browser);
		} else {
			System.out.println("Testcase TP_023 is fail in "+browser);
		}

	}

	@BeforeMethod
	public void verifyHomepage() {
		
		//Using actions class to go to settings page
		Actions goTo = new Actions(driver);
		goTo.moveToElement(driver.findElement(By.xpath("//span[@class='caret']"))).click().build().perform();

		driver.findElement(By.xpath("//span[contains(text(),'Settings')]")).click();

		// assertion to verify password setting page
		String expectedURL = "https://qatest.twoplugs.com/settings/Password";
		String actualURL = driver.getCurrentUrl();
		assertEquals(actualURL, expectedURL);
		System.out.println("You are on Password Setting page");
	}

	@AfterMethod
	public void afterMethod() {
		driver.manage().timeouts().implicitlyWait(8, TimeUnit.SECONDS);
		
		driver.findElement(By.xpath("//span[contains(text(),'Password')]")).click();
		
		//change password back to original
		password("test3plug", "test2plug");

	}

	@BeforeTest
	@Parameters({ "browser", "username", "password", "urlToLogin" })
	public void beforeTest(String browser, String username, String password, String urlToLogin) throws IOException {
		
		//Calling "InitializeDriver(browser)" method from BaseTest class
		driver = InitializeDriver(browser);
		
		// Initializing wait
		wait = new WebDriverWait(driver, 10);
		
		//Calling "logInTwoPlugs(driver, username, password, urlToLogin)" method from BaseTest Class to login
		logInTwoPlugs(driver, username, password, urlToLogin);

	}

	@AfterTest
	public void afterTest() {
		
		//Calling quitDriver(driver) method to quit the browser after executing the test
		quitDriver(driver);

	}

}
