//TP_018 Testing whether the refund icon is working properly

package twoPLUGS_testcases;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import static org.testng.Assert.assertEquals;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.AfterTest;

public class TP_018 extends BaseTestClass {
	WebDriver driver;
	WebDriverWait wait;
	String testCaseResult;

	@Test
	@Parameters({ "browser", "sellerUsername", "sellerPassword", "urlToLogin", "username", "password" })
	public void refund(String browser, String sellerUsername, String sellerPassword, String urlToLogin, String username,
			String password) {
		// Click on search icon
		driver.findElement(By.xpath("//span[@class='w-icons-search']")).click();

		// Enter service you want to search for and click on the service 
		driver.findElement(By.xpath("//input[@id='searchInput']")).sendKeys("paramTest Service" + Keys.ENTER);

		driver.findElement(By.xpath("//a[contains(text(),'ParamTest Service')]")).click();
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//span[contains(text(),'I want this')]"))));
		
		// Buy Service by clicking on "I Want this"
		driver.findElement(By.xpath("//span[contains(text(),'I want this')]")).click();
		
		//Accept agreement and click "Buy"
		WebElement checkbox = driver.findElement(By.xpath("//div[@id='chk_buyer_disclaimer-styler']"));
		wait.until(ExpectedConditions.visibilityOf(checkbox));
		checkbox.click();
		driver.findElement(By.xpath("//span[contains(text(),'Buy')]")).click();

		
		// logout and login into seller's account
		
		//Calling logOut(WebDriver driver) method from BaseTestClass to logout
		logOut(driver);

		// Calling "logInTwoPlugs(driver, username, password, urlToLogin)" method from BaseTest Class to login
		logInTwoPlugs(driver, sellerUsername, sellerPassword, urlToLogin);

		// go to messages
		driver.findElement(By.xpath("//span[contains(text(),'Messages')]")).click();

		//Click on message to accept bid
		driver.findElement(By.xpath("//tr[1]//td[2]//a[1]")).click();

		// Calling staticWait(Integer secs) method from BaseTest class
		staticWait(5);
		
		//Accept agreement and click confirm
		driver.findElement(By.xpath("//label[@id=\"checktermctr\"]")).click();
		// Calling staticWait(Integer secs) method from BaseTest class
		staticWait(2);
		driver.findElement(By.xpath("//a[@id='seller_confirm_btn']")).click();

		// Calling staticWait(Integer secs) method from BaseTest class
		staticWait(5);
		
		// Click on message to confirm delivery
		driver.findElement(By.xpath("//tr[1]//td[2]//a[1]")).click();
	
		// Calling staticWait(Integer secs) method from BaseTest class
		staticWait(5);
		
		//Click on Confirm button
		driver.findElement(By.xpath("//div[@id='message_confirm_button']/a")).click();

		// logout and login with buyer user
		// Calling staticWait(Integer secs) method from BaseTest class
		staticWait(5);
		
		//Calling logOut(WebDriver driver) method from BaseTestClass to logout
		logOut(driver);
		
		// Calling "logInTwoPlugs(driver, username, password, urlToLogin)" method from BaseTest Class to login
		logInTwoPlugs(driver, username, password, urlToLogin);

		// Go to transactions
		driver.findElement(By.xpath("//span[contains(text(),'Transactions')]")).click();

		// Ask refund
		
		//Clicking on Refund icon
		WebElement icon = driver
				.findElement(By.xpath("/html[1]/body[1]/div[7]/div[1]/table[2]/tbody[1]/tr[1]/td[7]/a[1]/i[1]"));
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		icon.click();

		//Waiting for pop-up and click on "I want a refund"
		wait.until(ExpectedConditions
				.visibilityOf(driver.findElement(By.xpath("//span[contains(text(),'I want a refund')]"))));
		driver.findElement(By.xpath("//span[contains(text(),'I want a refund')]")).click();

		//Waiting for confirmation pop-up 
		WebElement displayMsg = driver.findElement(By.xpath("//div[@id='refundmessagecontent']"));

		wait.until(ExpectedConditions.visibilityOf(displayMsg));
		
		//Assertion to verify confirmation pop-up
		Assert.assertEquals(true, displayMsg.isDisplayed());
		
		//Storing text of pop-up in a string
		String result = displayMsg.getText();
		
		//Printing results
		if (result.contains("Your refund request has been sent to the Seller.")) {
			System.out.println("Testcase TP_018 is pass in "+browser);
		} else {
			System.out.println("Testcase TP_018 is fail in "+browser);
		}

	}

	@BeforeMethod
	public void verifyHomepage() {
		String expectedURL = "https://qatest.twoplugs.com/home";
		String actualURL = driver.getCurrentUrl();
		System.out.println("You are on user's homepage TP18");
		// Assertion to verify that we are on User's homepage after successfully logging in
		assertEquals(actualURL, expectedURL);
	}

	@BeforeTest
	@Parameters({ "browser", "username", "password", "urlToLogin" })
	public void beforeTest(String browser, String username, String password, String urlToLogin) throws IOException {
		
		// Calling "InitializeDriver(browser)" method from BaseTest class
		driver = InitializeDriver(browser);
		
		// Initializing wait
		wait = new WebDriverWait(driver, 10);
		
		// Calling "logInTwoPlugs(driver, username, password, urlToLogin)" method from BaseTest Class to login
		logInTwoPlugs(driver, username, password, urlToLogin);

	}

	@AfterTest
	public void afterTest() {
		
		// Calling quitDriver(driver) method to quit the browser after executing the test
		quitDriver(driver);
	}

}
