//TP_004 Testing whether a user can search for a service, needs

package twoPLUGS_testcases;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;

import static org.testng.Assert.assertEquals;
import java.io.IOException;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.AfterTest;

public class TP_004 extends BaseTestClass {
	WebDriver driver;
	String baseURL;
	WebDriverWait myWait;

	@Test
	@Parameters({ "searchKeyword" })
	public void searchServiceNeed(String searchKeyword) {
		
		//Click on Search icon
		driver.findElement(By.xpath("//span[@class='w-icons-search']")).click();
		
		//Enter search keyword and press Enter key
		driver.findElement(By.xpath("//input[@id='searchInput']")).sendKeys(searchKeyword + Keys.ENTER);
	}

	@BeforeMethod
	public void verifyHomepage() {
		String expectedURL = "https://qatest.twoplugs.com/home";
		String actualURL = driver.getCurrentUrl();
		
		//Assertion to verify homepage
		assertEquals(actualURL, expectedURL);
		System.out.println("You are on user's homepage");

	}

	@AfterMethod
	@Parameters({ "searchKeyword", "browser" })
	public void afterMethod(String searchKeyword, String browser){

		//Calling wait method from BaseTest Class
		staticWait(5);   
		
		//Getting text of the search results and storing it in a string
		String result = driver.findElement(By.xpath("//tr[5]//td[1]//div[1]//div[2]//div[1]//a[1]")).getText();
		result = result.toLowerCase();
			
		//Assertion to verify testcase
		Assert.assertEquals(true, result.contains(searchKeyword));
		
		//Printing result
		if (result.contains(searchKeyword)) {
			System.out.println("TP_004 is Pass in " + browser);
		} else {
			System.out.println("TP_004 is Fail in " + browser);
		}
	}

	@BeforeTest
	@Parameters({ "browser", "username", "password", "urlToLogin" })
	public void beforeTest(String browser, String username, String password, String urlToLogin) throws IOException {
		
		//Calling InitializeDriver(browser) method from BaseTest class
		driver = InitializeDriver(browser);
		
		//Calling logInTwoPlugs method from BaseTest class
		logInTwoPlugs(driver, username, password, urlToLogin);

	}

	@AfterTest
	public void afterTest() {
		
		//Calling quitDriver(driver) method to quit the browser after executing the test
		quitDriver(driver);
	}

}
