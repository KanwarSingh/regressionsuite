//TP_024 Testing whether a user can refer a friend

package twoPLUGS_testcases;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;

import static org.testng.Assert.assertEquals;

import java.io.IOException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.AfterTest;

public class TP_024 extends BaseTestClass {
	WebDriver driver;
	WebDriverWait wait;
	WebElement displayMsg ;
	String result;

	@Test
	@Parameters({"referEmail"})
	public void referFriend(String referEmail) {
		
		//Using Actions class to go to Settings page
		Actions goTo = new Actions(driver);
		goTo.moveToElement(driver.findElement(By.xpath("//span[@class='caret']"))).click().build().perform();

		driver.findElement(By.xpath("//span[contains(text(),'Settings')]")).click(); 
		
		//Click on Refer
		driver.findElement(By.xpath("//span[contains(text(),'Refer a Friend')]")).click();
		
		//Entering refer email and click Send button
		driver.findElement(By.xpath("//input[@id='email_input']")).sendKeys(referEmail);
		
		driver.findElement(By.xpath("//span[contains(text(),'Send')]")).click();
		
		displayMsg= driver.findElement(By.xpath("//div[contains(text(),'Referral emails has been sent out')]"));
		
		wait.until(ExpectedConditions.visibilityOf(displayMsg));

		result = displayMsg.getText();
			
	}

	@BeforeMethod
	public void verifyHomeage() {
		
		String expectedURL = "https://qatest.twoplugs.com/home";
		String actualURL = driver.getCurrentUrl();
		
		//Assertion to verify that we are on User's homepage after successfully logging in
		assertEquals(actualURL, expectedURL);
		System.out.println("You are on user's Homepage");
	}

	@AfterMethod
	@Parameters({"browser"})
	public void afterMethod(String browser) {
		//Assertion to verify Pop-up message to confirm that referral email has been sent 
		
		//Assert.assertEquals(true, displayMsg.isDisplayed());
		
		
		//Printing results
		if (result.contains("Referral emails has been sent out")) {
			System.out.println("Testcase TP_024 is pass in "+browser);
		} else {
			System.out.println("Testcase TP_024 is fail in "+browser);
		}		

	}

	@BeforeTest
	@Parameters({ "browser", "username", "password", "urlToLogin" })
	public void beforeTest(String browser, String username, String password, String urlToLogin) throws IOException {
		
		//Calling "InitializeDriver(browser)" method from BaseTest class
		driver = InitializeDriver(browser);
		
		// Initializing wait
		wait = new WebDriverWait(driver, 10);
		
		//Calling "logInTwoPlugs(driver, username, password, urlToLogin)" method from BaseTest Class to login
		logInTwoPlugs(driver, username, password, urlToLogin);

	}

	@AfterTest
	public void afterTest() {
		
		// Calling quitDriver(driver) method to quit the browser after executing the test
		quitDriver(driver);

	}

}
