//TP_006 Testing the functionality of the four icons named as follow, sendeeds, send message, report user is working

package twoPLUGS_testcases;

import org.testng.annotations.Test;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;

public class TP_006 extends BaseTestClass {

	String goToURL;
	WebDriver driver;

	@BeforeMethod
	@Parameters("profileName")
	public void beforeMethod(String profileName) {
		String expectedURL = "https://qatest.twoplugs.com/profile/" + profileName + "#";
	String actualURL = driver.getCurrentUrl();
	System.out.println("you are on " + profileName + "'s profile page");
		//Assert.assertEquals(actualURL, expectedURL);
	}

	//Testing functionality of "follow" icon
	@Test
	@Parameters({ "browser" })
	public void follow(String browser) {
		
		//Creating an object of WebDriverWait class
		WebDriverWait wait = new WebDriverWait(driver, 30);

		//Getting title of the follow icon and storing it in a string "result"
		WebElement unfollowUser = driver.findElement(By.xpath("//a[@id='followuserlogo']"));
		String result = unfollowUser.getAttribute("title");
	
		boolean followAgain = true;
		
		//If title says "unfollow", the user is already followed. So, i am unfollowing the user
		if (result.contains("Unfollow")) {
			System.out.println("User is already followed");
			
			//Unfollowing the user by clicking on "follow" icon
			WebElement followIcon = driver.findElement(By.xpath("//span[@class='w-icons-profileCtrl1']"));
			followIcon.click();
			WebElement confirmUnfollow = driver.findElement(By.xpath("//span[contains(text(),'Confirm')]"));

			//Waiting for visibility of "Confirm Unfollow" pop-up
			wait.until(ExpectedConditions.visibilityOf(confirmUnfollow));
			
			//Confirming Unfollow
			confirmUnfollow.click();
			
			System.out.println("You have unfollowed the user in "+browser);

			followAgain = true;
		}
		
		//Following the user
		if (followAgain) {
			
			//Calling staticWait(Integer secs) method from BaseTest class
			staticWait(3);
			
			//Clicking on the "follow" Icon
			WebElement followIcon = driver.findElement(By.xpath("//span[@class='w-icons-profileCtrl1']"));
			wait.until(ExpectedConditions.visibilityOf(followIcon));
			followIcon.click();
			
			//Confirming follow
			WebElement confirmFollow = driver.findElement(By.xpath("//span[contains(text(),'Confirm')]"));
			
			//waiting foe visibility of Confirm follow pop-up
			wait.until(ExpectedConditions.visibilityOf(confirmFollow));
			
			driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			
			//Clicking on "Confirm follow"
			confirmFollow.click();

			//Waiting for the follow alert and getting its text
			WebElement followAlert = driver
					.findElement(By.xpath("//div [@role='alert' and  contains(text(),'following user')]"));
			
			wait.until(ExpectedConditions.visibilityOf(followAlert));
			
			result = followAlert.getText();
			//System.out.println(result);
			
			//Assertion to verify that user is successfully followed
			Assert.assertEquals(true,result.contains("following user"));
			
			
			//Printing results
			if (result.contains("following user")) {
				System.out.println("User is successfully followed in " + browser);

			}

			else {
				System.out.println("follow user failed in " + browser);
			}

		}

	}

	//Testing functionality of "send EEDS" icon
	@Test
	@Parameters({ "browser" })
	public void sendEeds(String browser) {
		
		//Creating an object of WebDriverWait class
		WebDriverWait wait = new WebDriverWait(driver, 30);

		//Clicking on "sendEEDS" icon
		WebElement sendEeds = driver.findElement(By.xpath("//span[@class='w-icons-profileCtrl2']"));
		sendEeds.click();
		
		//Entering amount to be sent
		WebElement enterAmount = driver.findElement(By.xpath("//input[@id='transferAmount']"));
		enterAmount.sendKeys("1");
		
		//Clicking on "Transfer" button
		driver.findElement(By.xpath("//span[contains(text(),'TRANSFER')]")).click();

		//Waiting for confirm transfer pop-up and clicking on "Transfer" button
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//a[@id='btn_transfer']//span[@class='help'][contains(text(),'Transfer')]"))));
		
		driver.findElement(By.xpath("//a[@id='btn_transfer']//span[@class='help'][contains(text(),'Transfer')]"))
				.click();

		//Confirming transfer from Transactions
		WebElement checkTransactions = driver.findElement(By.xpath("//span[contains(text(),'Transactions')]"));
		checkTransactions.click();
		
		//Getting text of message for the above transaction 
		WebElement findTransaction = driver.findElement(By.xpath("//tr[1]//td[5]//div[1]//div[1]"));
		String transaction = findTransaction.getText();
		
		//Assertion to verify transaction
		Assert.assertEquals(true, transaction.contains("1"));

		//Printing results
		if (transaction.contains("1")) {
			System.out.println("Transfer EEDS successful in " + browser);
		} else {
			System.out.println("Transfer EEDS Unsuccessful in " + browser);
		}

	}

	//Testing functionality of "send message" icon
	@Test
	@Parameters({ "browser" })
	public void sendMessage(String browser) {
		
		//Creating an object of WebDriverWait class
		WebDriverWait wait = new WebDriverWait(driver, 10);
		
		//Clicking on "message" icon
		WebElement message = driver.findElement(By.xpath("//span[@class='w-icons-profileCtrl3']"));
		message.click();
		
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		
		//Entering message title, content and clicking on "Send" button
		WebElement msgTitle = driver.findElement(By.xpath("//input[@id='messagetitle']"));
		msgTitle.sendKeys("Hi");
		
		WebElement content = driver.findElement(By.xpath("//textarea[@id='messagecontent']"));
		content.sendKeys("This is a test msg");
		
		driver.findElement(By.xpath("//button[@id='message_send']")).click();
		
		driver.navigate().refresh();
		
		//Verifying the sent message
		WebElement goToMessages = driver.findElement(By.xpath("//ul[@class='nav navbar-nav']//span[@class='help'][contains(text(),'Messages')]"));
		
		wait.until(ExpectedConditions.elementToBeClickable(goToMessages)).click();
		
		//Clicking on Sent Mail
		driver.findElement(By.xpath("//span[contains(text(),'Sent Mail')]")).click();
		
		//Verifying the sent message
		String result = driver.findElement(By.xpath("//a[contains(text(),'Hi')]")).getText();
		
		
		//Assertion to verify message
		Assert.assertEquals(true, result.contains("Hi"));

		//Printing results
		if (result.contains("Hi")) {
			System.out.println("Send message successful in "+browser);
		} else {
			System.out.println("Send message failed in "+browser);
		}

	}

	//Testing functionality of "report" icon
	@Test
	@Parameters({ "browser" })
	public void report(String browser) throws InterruptedException {

		//Creating an object of WebDriverWait class
		WebDriverWait wait = new WebDriverWait(driver, 30);
		
		//Clicking on "report" icon
		WebElement reportUser = driver.findElement(By.xpath("//span[@class='w-icons-profileCtrl4']"));
		
		wait.until(ExpectedConditions.visibilityOf(reportUser));
		
		reportUser.click();
		
		//Entering subject, content and click on "Submit Report" button
		WebElement subject = driver.findElement(By.xpath("//input[@id='reportSubject']"));
		
		wait.until(ExpectedConditions.visibilityOf(subject));

		subject.sendKeys("Test");
		
		WebElement content = driver.findElement(By.xpath("//textarea[contains(@placeholder,'Content')]"));
		content.sendKeys("This is a test report");
		
		driver.findElement(By.xpath("//span[contains(text(),'Submit report')]")).click();
		
		//Waiting for alert message and storing its text in a String "result"
		WebElement reportAlert = driver
				.findElement(By.xpath("//div[contains(text(),'Your complaint has been submitted. You will be con')]"));
		
		wait.until(ExpectedConditions.visibilityOf(reportAlert));
		
		String result = reportAlert.getText();
		
		System.out.println(result);
		
		//Assertion to verify report user pop-up
		Assert.assertEquals(true, result.contains("Your complaint has been submitted"));

		//Printing results
		if (result.contains("Your complaint has been submitted")) {
			System.out.println("User is successfully reported in "+browser);

		}

		else {
			System.out.println("report user failed in "+browser);

		}
	}

	@AfterMethod
	@Parameters({ "profileName", "urlToLogin" })
	public void afterMethod(String profileName, String urlToLogin) {
		
		//Navigating to user's profile page after executing each test
		String goToURL = urlToLogin + "/profile/" + profileName + "#";
		driver.navigate().to(goToURL);

	}

	@BeforeTest
	@Parameters({ "browser", "username", "password", "urlToLogin", "profileName" })
	public void beforeTest(String browser, String username, String password, String urlToLogin, String profileName)
			throws IOException {

		 //Calling "InitializeDriver(browser)" method from BaseTest class
		driver = InitializeDriver(browser);
		
		//Calling "logInTwoPlugs(driver, username, password, urlToLogin)" method from BaseTest Class to login
		logInTwoPlugs(driver, username, password, urlToLogin);
		
		//Navigating to user's profile
		String goToURL = urlToLogin + "/profile/" + profileName + "#";
		driver.navigate().to(goToURL);

	}

	@AfterTest
	public void afterTest() {
		
		//Calling quitDriver(driver) method to quit the browser after executing the test
		driver.quit();
	}

}
