//TP_007 Testing the functionality creating /updating/deleting a service

package twoPLUGS_testcases;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;

import static org.testng.Assert.assertEquals;

import java.io.IOException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.AfterTest;

public class TP_007 extends BaseTestClass{
	WebDriver driver;
	String goToURL = "https://qatest.twoplugs.com/home";
	WebDriverWait wait;
	static String profileLink;
	
	//Testing the functionality of Creating a Service
	@Test(priority=0)
	@Parameters({"browser"})
	public void createService(String browser) {
		
		//Click on "Create" and then seclect "Service"
		driver.findElement(By.xpath("//div[@class='create']")).click();
		driver.findElement(By.xpath("//a[contains(text(),'Service')]")).click();
		
		//Enter Service title
		WebElement title = driver.findElement(By.xpath("//input[@id='name']"));
		title.sendKeys("Laundry Service");
		
		//Enter Service description
		WebElement description = driver.findElement(By.xpath("//textarea[@id='description']"));
		description.sendKeys("Laundry Service by Joy");
		
		//select Service category
		driver.findElement(By.xpath("//div[@id='category_id-styler']//div[@class='jq-selectbox__trigger']")).click();
		WebElement selectCategory = driver.findElement(By.xpath("//li[contains(text(),'Home & Child')]"));
		selectCategory.click();
	
		//select Service Sub category
		if(browser.equalsIgnoreCase("chrome")) {
		WebElement subcategory= driver.findElement(By.xpath("//select[@id='subcategory_id']"));
		Select dropdown = new Select(subcategory);
		dropdown.selectByVisibleText("Laundry");
		}
		else {
			driver.findElement(By.xpath("//div[@id='subcategory_id-styler']//div[@class='jq-selectbox__trigger']")).click();
			staticWait(3);
			driver.findElement(By.xpath("//li[contains(text(),'Laundry')]")).click();
		}
		
		//Enter price, select refund, enter refund valid and click "Create"
		WebElement price = driver.findElement(By.xpath("//input[@id='price']"));
		price.sendKeys("20");
		
		WebElement refund = driver.findElement(By.xpath("//div[@id='slider-range-max3']"));
		refund.click();
		
		driver.findElement(By.xpath("//input[@id='refund_valid']")).sendKeys("10");
		
		driver.findElement(By.xpath("//span[contains(text(),'create')]")).click();
		
		//driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
		
		//Waiting for visibility of popup and storing its text in a string "result"
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//div[contains(text(),'Service has been added')]"))));
		String result = driver.findElement(By.xpath("//div[contains(text(),'Service has been added')]")).getText();
		
		//Verifying pop-up message
		Assert.assertEquals(result, "Service has been added");
		
		//Printing results
		if (result.contains("Service")) {
			System.out.println("Create service successful in "+browser);
		} else {
			System.out.println("Create service failed in "+browser);
		}
	}

	//Testing the functionality of Updating a Service
	@Test(priority=1)
	@Parameters({"browser"})
	public void updateService(String browser) {
		
		//Refresh and maximize page
		driver.navigate().refresh();
		driver.manage().window().maximize();
		
		//Calling staticWait(Integer secs) method from BaseTest class
		staticWait(4);
		
		//Using Actions class to move to an element that displays a dropdown menu upon moving to it
		Actions action = new Actions(driver);
		WebElement goTo = driver.findElement(By.xpath("//span[@class='caret']"));
		action.moveToElement(goTo).build().perform();
		
		//Click on Profile
		WebElement goToProfile = driver.findElement(By.xpath("//span[contains(text(),'Profile')]"));
		goToProfile.click();

		//Storing URL of profile in a static string "profileLink"
		profileLink=driver.getCurrentUrl();
		
		// go to laundry service
		driver.findElement(By.xpath("//div[@class='box theme-2']//tr[1]//td[1]//div[1]//a[1]")).click();
		
		//Click on "Edit"
		WebElement edit = driver.findElement(By.xpath("//a[@class='pull-right edit-link']"));
		edit.click();
		
		//Update the service title
		WebElement updateTitle = driver.findElement(By.xpath("//input[@id='name']"));
		updateTitle.clear();
		updateTitle.sendKeys("washing and laundry");
		
		//Click on "Save"
		driver.findElement(
				By.xpath("//ul[@class='line-btn pull-right']//button[@class='btn btn-success w-btn-success']")).click();
		//driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		
		//Waiting for pop-up and storing text in a string "result"
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//div[contains(text(),'Service has been updated')]"))));
		String result = driver.findElement(By.xpath("//div[contains(text(),'Service has been updated')]")).getText();
		
		//Assertion to verify pop-up
		Assert.assertEquals(result, "Service has been updated");

		//Printing results
		if (result.contains("updated")) {
			System.out.println("Service updated in "+browser);
		} else {
			System.out.println("Service not updated in "+browser);
		}
		
		//Waiting for pop-up to be invisible
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//div[contains(text(),'Service has been updated')]")));
	}
	
	//Testing the functionality of Deleting a Service
	@Test(priority=2)
	@Parameters({"browser"})
	public void deleteService(String browser) {
		try {
if(profileLink !=null)
{
			System.out.println("Profile link : "+ profileLink);
			
			//Navigating to profile by using static string "profileLink"
			driver.navigate().to(profileLink);
			
			//Calling staticWait(Integer secs) method from BaseTest class
			staticWait(5);
}
else
{
	//Refresh and maximize page
		driver.navigate().refresh();
		driver.manage().window().maximize();
		//staticWait(5);
		
		//Using Actions class to move to an element that displays a dropdown menu upon moving to it
		Actions action = new Actions(driver);
		driver.manage().window().maximize();
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[@href='/newplug']")));
		
		WebElement goTo = driver.findElement(By.xpath("//span[@class='caret']"));
		
		wait.until(ExpectedConditions.visibilityOf(goTo));
		
		action.moveToElement(goTo).click().build().perform();
		
		//Click on Profile
		WebElement goToProfile = driver.findElement(By.xpath("//span[contains(text(),'Profile')]"));
		goToProfile.click();
		
		System.out.println(driver.getCurrentUrl());
		}

//Click on Cross icon next to service name
		WebElement clickCross= driver.findElement(By.xpath("//div[@class=\"box theme-2\"]//a[@data-target=\"#deleteservice\"][1]"));
		wait.until(ExpectedConditions.visibilityOf(clickCross));
		clickCross.click();
		
		//Wait for pop-up to confirm delete and click on "I want to delete"
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//span[contains(text(),'I want to delete')]"))));
		driver.findElement(By.xpath("//span[contains(text(),'I want to delete')]")).click();
		
		//driver.manage().timeouts().implicitlyWait(6, TimeUnit.SECONDS);
		
		//Waiting for Pop-up to confirm delete and storing text in a string
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//div[contains(text(),'Service has been deleted')]"))));
		String result = driver.findElement(By.xpath("//div[contains(text(),'Service has been deleted')]")).getText();
		
		//Assertion to verify Pop-up
		Assert.assertEquals(result, "Service has been deleted");

		//Printing results
		if(result.contains("deleted")) {
			System.out.println("Service deleted in "+browser);
		}
		else {
			System.out.println("Delete service failed in "+browser);
		}	
		}
		catch (Exception e) {
			System.out.println(e.getMessage());

			Assert.assertEquals(true, false);
						// TODO: handle exception
		}
	}
	

	
	@BeforeMethod
	public void beforeMethod() {
		String expectedURL = "https://qatest.twoplugs.com/home";
		String actualURL = driver.getCurrentUrl();
		
		//Assertion to verify that we are on User's homepage after successfully logging in
		assertEquals(actualURL, expectedURL);
		System.out.println("You are on user's homepage");
	}

	@AfterMethod
	public void afterMethod() {
		
		//Navigating to "https://qatest.twoplugs.com/home" after executing each test
		driver.navigate().to(goToURL);
	}

	@BeforeTest
	@Parameters({"browser","username","password","urlToLogin"})
	public void beforeTest(String browser, String username, String password, String urlToLogin) throws IOException {
		
		//Calling "InitializeDriver(browser)" method from BaseTest class
		driver = InitializeDriver(browser);
		
		//Initializing wait
		wait= new WebDriverWait(driver, 30);
		
		//Calling "logInTwoPlugs(driver, username, password, urlToLogin)" method from BaseTest Class to login
		logInTwoPlugs(driver, username, password, urlToLogin);
	}
	
	@AfterTest
	public void afterTest() {
	
		//Calling quitDriver(driver) method to quit the browser after executing the test
		driver.quit();
	}

}
