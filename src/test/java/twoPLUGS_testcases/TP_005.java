//TP_005  Testing whether he canSearch for user 

package twoPLUGS_testcases;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;

import static org.testng.Assert.assertEquals;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.AfterTest;

public class TP_005 extends BaseTestClass {
	WebDriver driver;
	String baseURL;
	WebDriverWait myWait;
	boolean profileSearch = true;

	@Test
	@Parameters({ "profileName", "browser" })
	public void searchUser(String profileName, String browser) {

		// Enter search name in search box
	
		driver.findElement(By.xpath("//input[@id='exampleInputAmount']")).sendKeys(profileName.toLowerCase());

		// wait for suggestions to appear
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		// Creating a List of web elements named "allProfiles"
		List<WebElement> allProfiles = driver.findElements(By.xpath(
				"//div[@class=\"searchSuggest\"]//div[@class=\"suggestRow\" and contains (@data-href,\"profile\")]"));

		// using for each loop to check all the results in search suggest if they are
		// matching the search keyword
		if (allProfiles.size() > 0) {
			for (WebElement profile : allProfiles) {

				// Getting text of search results and storing in a String
				String userProfile = profile.findElement(By.tagName("strong")).getText();
				String username = profile.findElement(By.tagName("span")).getText();

				// Printing search results if the search suggest results are correct
				if (!(username.toLowerCase().contains(profileName)
						|| userProfile.toLowerCase().contains(profileName))) {
					System.out.println(
							"Fail as it is showing following result: " + profile.getAttribute("data-href").toString());
					profileSearch = false;
					break;
				} else {
					System.out.println(profile.getAttribute("data-href"));
				}

			}
		}
		/*
		 * //Clicking on the first search result that matches the profileName searched
		 * for for (int i = 0; i < allProfiles.size(); i++) {
		 * System.out.println(allProfiles.get(i).getText()); if
		 * (allProfiles.get(i).getText().equalsIgnoreCase(profileName)) {
		 * allProfiles.get(i).click(); break; } }
		 */

		//Clicking on the first search result that matches the profileName searched
		if (profileSearch) {
			System.out.println("Profile searched by user is " + allProfiles.get(0).getText());
			allProfiles.get(0).click();

			//Assertion to verify that the profile clicked is containing the profileName searched for in URL
			Assert.assertEquals(true, driver.getCurrentUrl().toLowerCase().contains(profileName));

			//Printing the results
			if (driver.getCurrentUrl().toLowerCase().contains(profileName)) {
				System.out.println("Search for user is successful in " + browser);
			} else {
				System.out.println("Search is Inappropriate. TC TP_005 fail in " + browser);
			}
		} else {
			System.out.println("no profiles found with " + profileName
					+ " as the first name. Search for another user in " + browser);

		}

	}

	@BeforeMethod
	public void verifyLandingPage() {
		String expectedURL = "https://qatest.twoplugs.com/home";
		String actualURL = driver.getCurrentUrl();
		
		//Assertion to verify that we are on User's homepage after successfully logging in
		assertEquals(actualURL, expectedURL);
		System.out.println("You are on user's home page");
	}

	
	@BeforeTest
	@Parameters({ "browser", "username", "password", "urlToLogin" })
	public void beforeTest(String browser, String username, String password, String urlToLogin) throws IOException {
		
		 //Calling "InitializeDriver(browser)" method from BaseTest class
		driver = InitializeDriver(browser);
		 
		//Calling "logInTwoPlugs(driver, username, password, urlToLogin)" method from BaseTest Class to login
		logInTwoPlugs(driver, username, password, urlToLogin);

	}

	@AfterTest
	
	public void afterTest() {
		
		//Calling quitDriver(driver) method to quit the browser after executing the test
		quitDriver(driver);
	}

}
