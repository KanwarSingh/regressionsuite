//TP_022 Testing whether a user can change the email through settings

package twoPLUGS_testcases;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;

import static org.testng.Assert.assertEquals;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.AfterTest;

public class TP_022 extends BaseTestClass {
	WebDriver driver;
	WebDriverWait wait;
	String currentEmail;
	
	//Method to Change email to "newEmailAddress"
	public String email(String newEmailAddress) {
		
		//Enter New Email
		WebElement newEmail = driver.findElement(By.xpath("//input[@name=\"new_email\"]"));
		newEmail.sendKeys(newEmailAddress);

		// Calling staticWait(Integer secs) method from BaseTest class
		staticWait(2);
		
		//Enter New Email to confirm
		WebElement confirmEmail = driver.findElement(By.xpath("//input[@name=\"confirm_email\"]"));
		confirmEmail.sendKeys(newEmailAddress);

		// Calling staticWait(Integer secs) method from BaseTest class
		staticWait(2);
		
		//Enter Current Password
		WebElement currentPassword = driver.findElement(By.xpath("//input[@name=\"new_email_password\"]"));
		currentPassword.sendKeys("test2plug");
		
		// Calling staticWait(Integer secs) method from BaseTest class
		staticWait(2);
		
		//Click on "Save Changes"
		driver.findElement(By.xpath("//span[contains(text(),'SAVE CHANGES')]")).click();
		
		//Waiting for confirmation Message and storing its text in a String 
		WebElement displayMsg = driver.findElement(By.xpath("//div[contains(text(),'Your email has been updated')]"));
		String result = displayMsg.getText();
		
		//Assertion to verify Confirmation message
		Assert.assertEquals(true, displayMsg.isDisplayed());
		
		//Returning String in which text of confirmation message is stored
		return result;
	}

	@Test
	@Parameters({"browser"})
	public void changeEmail(String browser) {

		//Calling email(String newEmailAddress) method to change email to "pk.testeng1111@gmail.com"
		String result=email("pk.testeng1111@gmail.com");
		
		//Printing results
		if(result.contains("email has been updated")) {
			System.out.println("Testcase TP_022 is pass in "+browser);
		} else {
			System.out.println("Testcase TP_022 is fail in "+browser);
		}
	}

	@BeforeMethod
	public void verifyHomepage() {
		
		//Using actions class to go to settings page
		Actions goTo = new Actions(driver);
		goTo.moveToElement(driver.findElement(By.xpath("//span[@class='caret']"))).click().build().perform();

		driver.findElement(By.xpath("//span[contains(text(),'Settings')]")).click();
		
		//Click on Email
		driver.findElement(By.xpath("//span[contains(text(),'Email')]")).click();

		// assertion to verify Email setting page
		String expectedURL = "https://qatest.twoplugs.com/settings/Email";
		String actualURL = driver.getCurrentUrl();
		assertEquals(actualURL, expectedURL);
		System.out.println("You are on Email Setting page");

		//Storing current email address in a string
		currentEmail= driver.findElement(By.xpath("//input[@id='signUpEmail']")).getAttribute("value");
		System.out.println("Current Email is "+currentEmail);
	}

	@AfterMethod
	public void afterMethod() {
		//Changing Email address back to original
		
		driver.manage().timeouts().implicitlyWait(8, TimeUnit.SECONDS);
		
		//Click on Email
		driver.findElement(By.xpath("//span[contains(text(),'Email')]")).click();

		//Calling String email(String newEmailAddress) method to change email 
		email(currentEmail);
			
	}

	@BeforeTest
	@Parameters({ "browser", "username", "password", "urlToLogin" })
	public void beforeTest(String browser, String username, String password, String urlToLogin) throws IOException {
		
		//Calling "InitializeDriver(browser)" method from BaseTest class
		driver = InitializeDriver(browser);
		
		// Initializing wait
		wait = new WebDriverWait(driver, 10);
		
		//Calling "logInTwoPlugs(driver, username, password, urlToLogin)" method from BaseTest Class to login
		logInTwoPlugs(driver, username, password, urlToLogin);

	}

	@AfterTest
	public void afterTest() {
		
		//Calling quitDriver(driver) method to quit the browser after executing the test
		quitDriver(driver);

	}

}
