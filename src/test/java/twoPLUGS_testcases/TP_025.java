//TP_025 Testing whether a user can sign out from twoplugs

package twoPLUGS_testcases;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;

import static org.testng.Assert.assertEquals;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.AfterTest;

public class TP_025 extends BaseTestClass {
	WebDriver driver;
	WebDriverWait myWait;

	@Test
	public void signOut() {

		// Using Actions class to display dropdown contents
		Actions goTo = new Actions(driver);
		WebElement target = driver.findElement(By.xpath("//span[@class='caret']"));
		goTo.moveToElement(target).click().build().perform();

		// click on signout
		driver.findElement(By.xpath("//span[contains(text(),'Sign Out')]")).click();

	}

	@BeforeMethod
	public void verifyHomepage() {
		String expected = "https://qatest.twoplugs.com/home";
		String actual = driver.getCurrentUrl();

		// Assertion to verify that we are on User's homepage after successfully logging
		// in
		assertEquals(actual, expected);
		System.out.println("You are on user's homepage");
	}

	@AfterMethod
	@Parameters({ "browser" })
	public void afterMethod(String browser) {
		// assertion to verify that user successfully logged out

		// Getting text of "LOG IN" button to confirm that user is on landing page
		WebElement login = driver.findElement(By.xpath(
				"//body[@class='main-template']/div[@class='wrapper']/header/div[@class='container']/ul[@class='control-bar']/li/a[@class='btn border']/span[1]"));
		String result = login.getText();

		System.out.println(result);

		Assert.assertEquals(true, result.contains("LOG IN"));

		// Printing results
		if (result.equalsIgnoreCase("LOG IN")) {
			System.out.println("Testcase TP_025 is pass in " + browser);
		} else {
			System.out.println("Testcase TP_025 is fail in " + browser);
		}
	}

	@BeforeTest
	@Parameters({ "browser", "username", "password", "urlToLogin" })
	public void beforeTest(String browser, String username, String password, String urlToLogin) throws IOException {

		// Calling "InitializeDriver(browser)" method from BaseTest class
		driver = InitializeDriver(browser);

		// Calling "logInTwoPlugs(driver, username, password, urlToLogin)" method from
		// BaseTest Class to login
		logInTwoPlugs(driver, username, password, urlToLogin);

	}

	@AfterTest
	public void afterTest() {
		// Calling quitDriver(driver) method to quit the browser after executing the test
		quitDriver(driver);
	}

}
