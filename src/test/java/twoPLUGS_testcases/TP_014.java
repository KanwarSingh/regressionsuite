//TP_014 Testing whether he can send eeds to another user by clciking on the  transfer icon

package twoPLUGS_testcases;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;

import static org.testng.Assert.assertEquals;
import java.io.IOException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.AfterTest;

public class TP_014 extends BaseTestClass {
	WebDriver driver;
	WebDriverWait myWait;

	@Test
	@Parameters({"browser","profileName"})
	public void sendEeds(String browser, String profileName) {
		
		//Clicking on "sendEEDS" icon
		WebElement sendEeds = driver.findElement(By.xpath("//span[@class='w-icons-profileCtrl2']"));
		sendEeds.click();
		
		//Entering amount to be sent
		WebElement enterAmount = driver.findElement(By.xpath("//input[@id='transferAmount']"));
		enterAmount.sendKeys("1");
		
		//Clicking on "Transfer" button
		driver.findElement(By.xpath("//span[contains(text(),'TRANSFER')]")).click();

		//Waiting for confirm transfer pop-up and clicking on "Transfer" button
		myWait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//a[@id='btn_transfer']//span[@class='help'][contains(text(),'Transfer')]"))));
		driver.findElement(By.xpath("//a[@id='btn_transfer']//span[@class='help'][contains(text(),'Transfer')]")).click();

		//Confirming transfer from Transactions
		WebElement checkTransactions = driver.findElement(By.xpath("//span[contains(text(),'Transactions')]"));

		myWait.until(ExpectedConditions.visibilityOf(checkTransactions));
		
		checkTransactions.click();
		
		//Getting text of message for the above transaction 
		WebElement findTransaction = driver.findElement(By.xpath("//tr[1]//td[5]//div[1]//div[1]"));
		String transaction = findTransaction.getText();
		
		//Assertion to verify transaction
				Assert.assertEquals(true, transaction.contains("1"));

		//Printing results
		if (transaction.contains("1")) {
			System.out.println("Transfer EEDS successful in "+browser);
		} else {
			System.out.println("Transfer EEDS Unsuccessful in "+browser);
		}

	}

	@BeforeMethod
	@Parameters({"profileName"})
	public void verifyPage(String profileName) {

		String expextedURL = "https://qatest.twoplugs.com/profile/" + profileName + "#";
		String actualURL = driver.getCurrentUrl();
		
		//Assertion to verify that we are on User's profile page 
		assertEquals(actualURL, expextedURL);
		System.out.println("you are on user's profile page");

	  }


	@BeforeTest
	@Parameters({ "browser", "username", "password", "urlToLogin","profileName" })
	public void beforeTest(String browser, String username, String password, String urlToLogin, String profileName) throws IOException {
		
		// Calling "InitializeDriver(browser)" method from BaseTest class
		driver = InitializeDriver(browser);
		
		//Calling "logInTwoPlugs(driver, username, password, urlToLogin)" method from BaseTest Class to login
		logInTwoPlugs(driver, username, password, urlToLogin);
		
		// Initializing wait
		myWait = new WebDriverWait(driver, 10);
		
		//Navigating to user's profile
		String goToURL = urlToLogin + "/profile/" + profileName + "#";
		driver.navigate().to(goToURL);

	}

	@AfterTest
	public void afterTest() {
		// Calling quitDriver(driver) method to quit the browser after executing the test
		quitDriver(driver);
	}

}
