//TP_021 Testing whether the footer links are taking to the appropriate page

package twoPLUGS_testcases;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;

import static org.testng.Assert.assertEquals;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.AfterTest;

public class TP_021 extends BaseTestClass{
	WebDriver driver;
	WebDriverWait wait;
	Boolean feedbackPage;
	Boolean supportPage;
	Boolean privacyPage;
	Boolean termsPage;
  @Test
  @Parameters({"browser"})
  public void footerLinks(String browser) {
	  
	  //Checking feedback footer link
	  WebElement feedback = driver.findElement(By.xpath("//a[contains(text(),'Feedback')]"));
	  
	  //Click on feedback
	  feedback.click();
	  
	  //assertion to verify that clicking on "feedback" is taking to the appropriate page
	  String expected = "https://qatest.twoplugs.com/contactus/feedback";
	  String actual= driver.getCurrentUrl();
	  Assert.assertEquals(actual, expected);
	  
	  //Printing results
	  if(expected.equals(actual)) {
		  System.out.println("You are on feedback page");
		  feedbackPage = true;	  
	  }
	  else {
		  System.out.println("You are not on feedback page");
		  feedbackPage=false;
	  }
	  
	  //Checking Support footer link
	  WebElement support = driver.findElement(By.xpath("//a[contains(text(),'Support')]"));
	  
	  //Click on Support
	  support.click();
	  
	  //assertion to verify that clicking on "Support" is taking to the appropriate page
	  String exp = "https://qatest.twoplugs.com/help";
	  String act = driver.getCurrentUrl();
	  
	  Assert.assertEquals(act, exp);

	  //Printing Results
	  if(exp.equals(act)) {
		  System.out.println("You are on support page");
		  supportPage=true;
	  }
	  else {
		  System.out.println("You are not on support page");
		  supportPage=false;
	  }
	  
	  //Checking Privacy Policy footer link
	  WebElement privacyPolicy = driver.findElement(By.xpath("//a[contains(text(),'Privacy policy')]"));
	 
	  //Click on "Privacy Policy"
	  privacyPolicy.click();
	  
	  //assertion to verify that clicking on "Privacy Policy" is taking to the appropriate page
	  String expctd = "https://qatest.twoplugs.com/terms#privacy";
	  String actul= driver.getCurrentUrl();
	  
	  Assert.assertEquals(actul, expctd);

	  //Printing results
	  if(expctd.equals(actul)) {
		  System.out.println("You are on privacy policy page");
		  privacyPage=true;
	  }
	  else {
		  System.out.println("You are not on Privacy policy page");
		  privacyPage=false;
	  }
	  
	  //Checking Terms of Use footer link
	  WebElement termsOfUse= driver.findElement(By.xpath("//a[contains(text(),'Terms of use')]"));
	 
	  //Click on "Terms of Use"
	  termsOfUse.click();
	  
	  //assertion to verify that clicking on "Terms of Use" is taking to the appropriate page
	  String ex = "https://qatest.twoplugs.com/terms";
	  String ac= driver.getCurrentUrl();
	  
	  Assert.assertEquals(ac, ex);

	  //Printing results
	  if(ex.equals(ac)) {
		  System.out.println("You are on terms page");
		  termsPage=true;	  
	  }
	  else {
		  System.out.println("You are not on terms page");
		  termsPage = false;
	  }
	  
	  
	  //Printing Final result for the Testcase
	  if(feedbackPage && supportPage && privacyPage && termsPage) {
		  System.out.println("Testcase TP_021 is Pass in "+browser);
	  }
	  else {
		  System.out.println("Testcase TP_021 is Fail in "+browser);
	  }
  }
  
  @BeforeMethod
  public void verifyHomepage() {
	  String expectedURL = "https://qatest.twoplugs.com/home";
		String actualURL = driver.getCurrentUrl();
		
		// Assertion to verify that we are on User's homepage after successfully logging in
		assertEquals(actualURL, expectedURL);
		System.out.println("You are on user's homepage");}


  @BeforeTest
  @Parameters({"browser","username","password","urlToLogin"})
  public void beforeTest(String browser, String username, String password, String urlToLogin) throws IOException {
	 
	// Calling "InitializeDriver(browser)" method from BaseTest class
	  driver=InitializeDriver(browser);
	 
	// Initializing wait
	 wait = new WebDriverWait(driver, 10);
	 
	//Calling "logInTwoPlugs(driver, username, password, urlToLogin)" method from BaseTest Class to login
	  logInTwoPlugs(driver, username, password, urlToLogin);
	  
	  
  }

  @AfterTest
  public void afterTest() {
	  
	//Calling quitDriver(driver) method to quit the browser after executing the test
	  quitDriver(driver);
	  
  }

}
