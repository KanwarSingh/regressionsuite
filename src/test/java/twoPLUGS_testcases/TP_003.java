//TP_003 Login Page > InValid Email & password

package twoPLUGS_testcases;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;

import static org.testng.Assert.assertEquals;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.AfterTest;

public class TP_003 extends BaseTestClass {
	WebDriver driver;
	String baseURL;

	@Test
	@Parameters({ "invalidEmail", "password" })
	public void InvalidEmailPassword(String invalidEmail, String password) {
		// Click on login button
		WebElement logIn = driver.findElement(By.xpath(
				"//body[@class='main-template']/div[@class='wrapper']/header/div[@class='container']/ul[@class='control-bar']/li/a[@class='btn border']/span[1]"));
		logIn.click();

		// Enter username password
		WebElement username = driver.findElement(By.xpath("//input[@id='signInEmail']"));
		username.sendKeys(invalidEmail);

		WebElement pswd = driver.findElement(By.xpath("//input[@id='signInPassword']"));
		pswd.sendKeys(password);

		// Click login
		driver.findElement(By.xpath("//ul[@class='line-btn']//button[@class='btn btn-success w-btn-success']")).click();

	}

	@BeforeMethod
	public void verifyLandingPage() {
		String expectedURL = "https://qatest.twoplugs.com/";
		String actualURL = driver.getCurrentUrl();
		
		//assertion to verify landing page
		assertEquals(actualURL, expectedURL);
		System.out.println("You are on twoPLUGS landing page");
	}

	@AfterMethod
	@Parameters({"browser"})
	public void afterMethod(String browser) {

		// assertion to verify Testcase
		String result = driver.findElement(By.xpath("//div[@class='alert alert-danger text-center']")).getText();
		String expected = "INVALID EMAIL/PASSWORD.";
		
		Assert.assertEquals(result, expected);
		
		//Printing result
		if (result.contains("INVALID")) {
			System.out.println("Testcase TP_003 is Pass in "+browser);
		} else {
			System.out.println("Testcase TP_003 is Fail in "+browser);
		}
	}

	@BeforeTest
	@Parameters({ "browser","urlToLogin" })
	public void launchBrowser(String browser, String urlToLogin) throws IOException {
	
		 //Calling InitializeDriver(browser) method from BaseTest class
		driver = InitializeDriver(browser);
		
		//Going to url to login 
		driver.get(urlToLogin);

	}

	@AfterTest
	public void afterTest() {
		
		//Calling quitDriver(driver) method to quit the browser after executing the test
		quitDriver(driver);
	}

}
