//TP_001 Signup_invalid user

package twoPLUGS_testcases;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;

import static org.testng.Assert.assertEquals;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.AfterTest;

public class TP_001 extends BaseTestClass {
	WebDriver driver;
	String baseURL;

	@Test
	@Parameters({ "signUpUser", "signUpEmail", "signUpPassword" })
	public void signUpInvalidUser(String signUpUser, String signUpEmail, String signUpPassword) {
		// Click on login button
		WebElement logIn = driver.findElement(By.xpath(
				"//body[@class='main-template']/div[@class='wrapper']/header/div[@class='container']/ul[@class='control-bar']/li/a[@class='btn border']/span[1]"));
		logIn.click();

		// click on Sign up button
		driver.findElement(By.xpath("//span[contains(text(),'SIGN UP')]")).click();

		// Enter username, email and password
		WebElement username = driver.findElement(By.xpath("//input[@id='signUpUser']"));
		username.sendKeys(signUpUser);

		WebElement email = driver.findElement(By.xpath("//input[@id='signUpEmail']"));
		email.sendKeys(signUpEmail);

		WebElement password = driver.findElement(By.xpath("//input[@id='signUpPassword']"));
		password.sendKeys(signUpPassword);

		// click on sign up button
		driver.findElement(By.xpath("//button[@class='btn btn-success w-btn-success wide']")).click();

	}

	@BeforeMethod
	public void verifyLandingPage() {
		String expectedURL = "https://qatest.twoplugs.com/";
		String actualURL = driver.getCurrentUrl();
		
		//assertion to verify landing page
		assertEquals(actualURL, expectedURL);
		System.out.println("You are on twoPLUGS landing page");
	}

	@AfterMethod
	@Parameters({ "browser" })
	public void afterMethod(String browser) {
		// assertion to verify testcase
		String expectedResult = "Username cannot contain special characters";
		String result = driver
				.findElement(By.xpath("//p[contains(text(),'Username cannot contain special characters')]")).getText();
		
		Assert.assertEquals(result, expectedResult);
		
		//Printing result
		if (result.contains(expectedResult)) {
			System.out.println("Testcase TP_001 is Pass in " + browser);
		} else {
			System.out.println("Testcase TP_001 is Fail in " + browser);
		}
	}

	@BeforeTest
	@Parameters({ "browser","urlToLogin" })
	public void launchBrowser(String browser, String urlToLogin) throws IOException {
		
		 //Calling InitializeDriver(browser) method from BaseTest class
		driver = InitializeDriver(browser); 
		
		//Going to url to login 
		driver.get(urlToLogin);         
	}

	@AfterTest
	public void closeBrowser() {
		
		//Calling quitDriver(driver) method to quit the browser after executing the test
		quitDriver(driver);
	}

}
