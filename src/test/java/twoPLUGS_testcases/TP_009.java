//TP_009 Editing the profile

package twoPLUGS_testcases;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;

import static org.testng.Assert.assertEquals;
import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.AfterTest;

public class TP_009 extends BaseTestClass {
	WebDriver driver;
	String baseURL;

	@Test
	@Parameters({"browser"})
	public void editProfile(String browser) {
		// Go to Profile using Actions class
		Actions goTo = new Actions(driver);
		goTo.moveToElement(driver.findElement(By.xpath("//span[@class='caret']"))).click().build().perform();

		driver.findElement(By.xpath("//span[contains(text(),'Profile')]")).click();

		// click on Edit profile
		driver.findElement(By.xpath("//a[contains(text(),'Edit Profile')]")).click();

		WebDriverWait wait = new WebDriverWait(driver, 5);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//label[contains(text(),'First Name')]")));

		//change State
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//div[@id='stateDropdown-styler']//div[@class='jq-selectbox__trigger']"))));
				driver.findElement(By.xpath("//div[@id='stateDropdown-styler']//div[@class='jq-selectbox__trigger']")).click();
				
				WebElement selectState = driver.findElement(By.xpath("//li[contains(text(),'Arkansas')]"));
				selectState.click();
		
				//Calling staticWait(Integer secs) method from BaseTest class
				staticWait(3);
				
		// change city/town to Rogers
		driver.findElement(By.xpath("//div[@id='cityDropdown-styler']//div[@class='jq-selectbox__trigger-arrow']"))
				.click();

		WebElement selectCity = driver.findElement(By.xpath("//li[contains(text(),'Rogers')]"));
		selectCity.click();

		//Click on "Save changes"
		driver.findElement(By.xpath("//span[contains(text(),'SAVE CHANGES')]")).click();

		//Waiting for changes to be saved
		WebDriverWait myWait = new WebDriverWait(driver, 5);
		myWait.until(ExpectedConditions.urlToBe("https://qatest.twoplugs.com/profile/joy"));

		//Assertion to verify that changes are reflected in profile
		String result = driver.findElement(By.xpath("//li[@class='wide']")).getText();
		
		Assert.assertEquals(true, result.contains("Rogers"));
		
		//Printing results
		if (result.contains("Rogers")) {
			System.out.println("Testcase TP_009 is pass in "+browser);
		} else {
			System.out.println("Testcase TP_009 is fail in "+browser);
		}
	}

	@BeforeMethod
	public void beforeMethod() {
		String expectedURL = "https://qatest.twoplugs.com/home";
		String actualURL = driver.getCurrentUrl();
		
		//Assertion to verify that we are on User's homepage after successfully logging in
		assertEquals(actualURL, expectedURL);
		System.out.println("You are on user's homepage");

	}


	@BeforeTest
	@Parameters({"browser","username","password","urlToLogin"})
	public void beforeTest(String browser, String username, String password, String urlToLogin) throws IOException {
		
		//Calling "InitializeDriver(browser)" method from BaseTest class
		driver = InitializeDriver(browser);
	
		//Calling "logInTwoPlugs(driver, username, password, urlToLogin)" method from BaseTest Class to login
		logInTwoPlugs(driver, username, password, urlToLogin);
	}

	@AfterTest
	public void afterTest() {
		
		//Calling quitDriver(driver) method to quit the browser after executing the test
		quitDriver(driver);
	}

}
