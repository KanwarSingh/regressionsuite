//TP_013 Testing whether he can bid the  the need by clicking on Bid button

package twoPLUGS_testcases;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;

import static org.testng.Assert.assertEquals;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.AfterTest;

public class TP_013 extends BaseTestClass {
	WebDriver driver;
	WebDriverWait myWait;

	@Test
	 @Parameters({"browser","searchName"})
	public void bidNeed(String browser, String searchName) {
		 //click on search icon
		  driver.findElement(By.xpath("//span[@class='w-icons-search']")).click();
		  
		  //Enter need you want to bid for, click search
		  WebElement searchInput= driver.findElement(By.xpath("//input[@id='searchInput']"));
		  searchInput.sendKeys(searchName);
		  driver.findElement(By.xpath("//span[contains(text(),'SEARCH')]")).click();
		  
		  driver.findElement(By.xpath("//i[@class='tps tps-9']")).click();
		  
		  //Click on "I can do this"
		  
		  myWait.until(ExpectedConditions.visibilityOf( driver.findElement(By.xpath("//span[contains(text(),'I can do this')]"))));

		  driver.findElement(By.xpath("//span[contains(text(),'I can do this')]")).click();
		  
		  //Enter price and service
		  
		  myWait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//input[@id='price']"))));
		  
		  driver.findElement(By.xpath("//input[@id='price']")).sendKeys("25");
		  driver.findElement(By.xpath("//form[@id='purchase_contract_form']//div[@id='service-styler']//div[@class='jq-selectbox__trigger-arrow']")).click();
		  driver.findElement(By.xpath("//form[@id='purchase_contract_form']//li[contains(text(),'jose service')]")).click();
		  
		  //accept agreement and send
		  
		  myWait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//div[@id='agreeterm-styler']"))));
		  
		  driver.findElement(By.xpath("//div[@id='agreeterm-styler']")).click();
		  driver.findElement(By.xpath("//span[contains(text(),'send')]")).click();
		  
		  //assertion to verify testcase
		
		  String result= driver.findElement(By.xpath("//h3[contains(text(),'Bidding Sent')]")).getText();

		  Assert.assertEquals(result, "Bidding Sent");
		  
		  //Printing results
		  if(result.contains("Bidding Sent")) {
			  System.out.println("Testcase TP_013 is pass in "+browser);
		  }
		  else {
			  System.out.println("Testcase TP_013 is fail in "+browser);
		  }
		
	}

	@BeforeMethod
	public void verifyHomepage() {
		  String expectedURL = "https://qatest.twoplugs.com/home";
			String actualURL = driver.getCurrentUrl();
			
			//Assertion to verify that we are on User's homepage after successfully logging in
			assertEquals(actualURL, expectedURL);
			System.out.println("You are on user's homepage");

	  }


	@BeforeTest
	@Parameters({ "browser", "username", "password", "urlToLogin" })
	public void beforeTest(String browser, String username, String password, String urlToLogin) throws IOException {
		
		// Calling "InitializeDriver(browser)" method from BaseTest class
		driver = InitializeDriver(browser);
		
		//Calling "logInTwoPlugs(driver, username, password, urlToLogin)" method from BaseTest Class to login
		logInTwoPlugs(driver, username, password, urlToLogin);

		// Initializing wait
		myWait = new WebDriverWait(driver, 10);
	}

	@AfterTest
	public void afterTest() {
		
		// Calling quitDriver(driver) method to quit the browser after executing the test
		quitDriver(driver);
	}

}
