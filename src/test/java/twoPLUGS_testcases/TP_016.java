//TP_016 Testing whether he can  add service match to his settings

package twoPLUGS_testcases;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import static org.testng.Assert.assertEquals;
import java.io.IOException;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.AfterTest;

public class TP_016 extends BaseTestClass {
	WebDriver driver;
	WebDriverWait wait;
	String testCaseResult;

	@Test
	@Parameters({ "browser" })
	public void addServiceMatch(String browser) {
		// Using actions class to go to settings page
		Actions goTo = new Actions(driver);
		goTo.moveToElement(driver.findElement(By.xpath("//span[@class='caret']"))).click().build().perform();

		driver.findElement(By.xpath("//span[contains(text(),'Settings')]")).click();

		driver.manage().window().maximize();

		// click on service match
		driver.findElement(By.xpath("//span[contains(text(),'Service Match')]")).click();

		// select type of match
		WebElement match = driver
				.findElement(By.xpath("//div[@id='match_type-styler']//div[@class='jq-selectbox__trigger']"));
		match.click();
		driver.findElement(By.xpath("//li[contains(text(),'Service')]")).click();

		// Scrolloing page down
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,1000)");

		// Select category
		WebElement category = driver
				.findElement(By.xpath("//div[@id='category_id-styler']//div[@class='jq-selectbox__trigger-arrow']"));
		category.click();
		driver.findElement(By.xpath("//li[contains(text(),'Computer & Phone')]")).click();
		WebElement categoryDropdown = driver
				.findElement(By.xpath("//div[@id='category_id-styler']//div[@class='jq-selectbox__dropdown']"));

		wait.until(ExpectedConditions.invisibilityOf(categoryDropdown));

		// Select sub category
		driver.findElement(By.xpath("//label[contains(text(),'SUB CATEGORY')]")).click();

		// Calling staticWait(Integer secs) method from BaseTest class
		staticWait(4);

		WebElement subcat = driver
				.findElement(By.xpath("//div[@id='subcategory_id-styler']//div[@class='jq-selectbox__trigger-arrow']"));
		wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.xpath("//div[@id='subcategory_id-styler']//div[@class='jq-selectbox__trigger-arrow']")));

		subcat.click();
		if (browser.equalsIgnoreCase("chrome")) {
			driver.findElement(By.xpath("//li[contains(text(),'Developer')]")).click();
		} else {
			driver.findElement(By.xpath("//li[contains(text(),'App')]")).click();

		}
		WebElement subcatDropdown = driver
				.findElement(By.xpath("//div[@id='subcategory_id-styler']//div[@class='jq-selectbox__dropdown']"));

		wait.until(ExpectedConditions.invisibilityOf(subcatDropdown));

		// Click add
		WebElement add = driver.findElement(By.xpath("//span[contains(text(),'add')]"));
		add.click();

		// Waiting for confirmation pop-up
		wait.until(ExpectedConditions.visibilityOf(
				driver.findElement(By.xpath("//body/div[@id='dismiss']/div[@class='modal-dialog']/div[1]"))));
		String result = driver.findElement(By.xpath("//body/div[@id='dismiss']/div[@class='modal-dialog']/div[1]"))
				.getText();

		// assertion to verify Service match pop-up
		Assert.assertEquals(result, "A match has been added");

		// Printing results
		if (result.contains("match has been added")) {
			testCaseResult = "Pass";
			System.out.println("Testcase TP_016 is pass in " + browser);
		} else {
			testCaseResult = "Fail";
			System.out.println("Testcase TP_016 is fail in " + browser);
		}

		// Calling staticWait(Integer secs) method from BaseTest class
		staticWait(4);

	}

	@BeforeMethod
	public void verifyHomepage() {
		String expectedURL = "https://qatest.twoplugs.com/home";
		String actualURL = driver.getCurrentUrl();

		// Assertion to verify that we are on User's homepage after successfully logging
		// in
		assertEquals(actualURL, expectedURL);
		System.out.println("You are on user's homepage");
	}

	@AfterMethod
	public void afterMethod() {

		// Deleting the match added in above @test

		// Creating a web element for delete icon
		WebElement deleteMatch = driver
				.findElement(By.xpath("//ul[@class='match-marketing-list']/li[last()]/a[@class='delete-li-item']"));
		wait.until(ExpectedConditions.visibilityOf(deleteMatch));

		//If match is successfully added, delete match
		if (testCaseResult.equalsIgnoreCase("Pass")) {
			deleteMatch.click();

		}

	}

	@BeforeTest
	@Parameters({ "browser", "username", "password", "urlToLogin" })
	public void beforeTest(String browser, String username, String password, String urlToLogin) throws IOException {
		
		// Calling "InitializeDriver(browser)" method from BaseTest class
		driver = InitializeDriver(browser);
		
		// Initializing wait
		wait = new WebDriverWait(driver, 30);
		
		// Calling "logInTwoPlugs(driver, username, password, urlToLogin)" method from BaseTest Class to login
		logInTwoPlugs(driver, username, password, urlToLogin);

	}

	@AfterTest
	public void afterTest() {
		
		// Calling quitDriver(driver) method to quit the browser after executing the test
		quitDriver(driver);
	}

}
